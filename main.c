#include <stdio.h>
#include <stdlib.h>
void readMatrix(int m,int n,float *ptr);
void printMatrix(int m,int n,float *ptr);
void gaussElimination(int m,int n,float *ptr);

int main() {
    int m,n;
    printf("\ninsert row number:");
    scanf("%d",&m);
    printf("insert column number:");
    scanf("%d",&n);
    float *ptr=(float*)malloc(m*n*sizeof(float));
    if(ptr !=NULL){
        printf("memory allocated %d byte\n",(int)m*n*sizeof(float));
    }else{
        printf("error during allocation");
        return -1;
    }
    readMatrix(m,n,ptr);
    gaussElimination(m,n,ptr);
    printMatrix(m,n,ptr);
    free(ptr);
    return 0;
}
void readMatrix(int m,int n,float *ptr){
    int i,j;
    for (i=0;i<m;i++){
        for(j=0;j<n;j++){
            printf("insert  M[%d][%d]:",i,j);
            scanf("%f",&ptr[i*m+j]);
        }
    }
}
void printMatrix(int m,int n,float *ptr){
    int i,j;
    for (i=0;i<m;i++){
        for(j=0;j<n;j++){
            printf("%f ",ptr[i*m+j]);
        }
        printf("\n");
    }
}

void gaussElimination(int m,int n,float *ptr){
    int i,j,k,pivot_row,pivot_column;
    printf("input matrix:\n");
    printMatrix(m,n,ptr);
    printf("\n");
    for(i=0;i<m-1;i++){//itera righe per meg
        printf("step %d\n",i);
        k=i;
        j=i;
        while(j<n && ptr[k*m+j]==0){//iter to find pivot
            while(k<m &&  ptr[k*m+j]==0){
                if(ptr[k*m+j]==0)
                k++;
            }
            if(k==m)
                k = i;
            if(ptr[k*m+j]==0) {
                j++;
            }
        }
        if(j==n)
            return;
        pivot_row=k;
        pivot_column=j;
        if(k!=i){//if pivot is not on column i swap
            printf("swap\n");
            for(j=0;j<n;j++){
                float tmp=ptr[i*m+j];
                ptr[i*m+j]=ptr[k*m+j];
                ptr[k*m+j]=tmp;
            }
            pivot_row=i;
        }
        printf("\npivot[%d][%d]:%f\n",pivot_column,pivot_row,ptr[pivot_row*m+pivot_column]);
        for(j=i+1;j<m;j++){
            float mult_term=ptr[j*m+pivot_column]/ptr[pivot_row*m+pivot_column];
            for(k=0;k<n;k++){
                ptr[j*m+k]-=mult_term*ptr[i*m+k];
            }
        }
        printf("after gauss \n");
        printMatrix(m,n,ptr);
        printf("\n");

    }
}